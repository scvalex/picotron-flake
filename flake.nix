{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs =
    { self, nixpkgs, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
      libPath =
        with pkgs;
        lib.makeLibraryPath [
          libGL
          libpulseaudio
          SDL2
          xorg.libXxf86vm
          xorg.libX11
        ];
      version = "0.1.1e";
    in
    {
      apps.${system}.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/picotron";
      };

      packages.${system}.default =
        with pkgs;
        stdenv.mkDerivation {
          name = "picotron";
          version = version;

          nativeBuildInputs = [
            copyDesktopItems
            makeWrapper
            unzip
          ];

          desktopItems = [
            (makeDesktopItem {
              name = "picotron";
              desktopName = "Picotron";
              genericName = "Fantasy Workstation";
              icon = "picotron";
              exec = "picotron";
              categories = [ "Development" ];
            })
          ];

          src = requireFile {
            name = "picotron_${version}_amd64.zip";
            message = ''
              This nix expression requires that picotron_${version}_amd64.zip is
              already part of the store. Download the file from
              https://www.lexaloffle.com/games.php?page=updates
              and add it to the nix store with nix-store --add-fixed sha256 <FILE>.
            '';
            # Generate the hash with `nix hash file ~/Downloads/picotron_0.1.1_amd64.zip`
            sha256 = "sha256-AR9xrM9ocO8z29HVQRzzvHt+9OZ192a2tFVw+ByaDo8=";
          };

          dontConfigure = true;
          dontBuild = true;
          dontStrip = true;

          installPhase = ''
            runHook preInstall
            mkdir -p $out/bin
            patchelf \
              --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
              picotron
            mv picotron $out/bin/
            mv picotron.dat $out/bin/
            mv picotron_dyn $out/bin/
            wrapProgram $out/bin/picotron \
              --prefix LD_LIBRARY_PATH : ${libPath}
            install -Dm644 lexaloffle-picotron.png $out/share/pixmaps/picotron.png
            runHook postInstall
          '';
        };
    };
}
