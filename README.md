picotron-flake
==============

> A NixOS flake to run Lexalloffle's [Picotron](https://www.lexaloffle.com/picotron.php)

## Usage

First, download the Picotron binary zip from the [Downloads page on
your Lexaloffle
account](https://www.lexaloffle.com/games.php?page=updates) and add it
to the Nix store:

```
$ nix-store --add-fixed sha256 ~/Downloads/picotron_0.1.1e_amd64.zip
```

Afterwards, you can just run the flake with:

```
$ nix run gitlab:scvalex/picotron-flake/0.1.1e
```

I'll be using git tags to identify different versions.  So, if you
want to run 0.1.0g, just replace the tag part of the above command.

If you need to run a version not included in this repository, update
the `version`, `name`, and `sha256` parts of the `flake.nix`.  You can
get the `sha256` value by running `nix hash file` on the downloaded
file.

## Troubleshooting

### 0.1.0f

Version 0.1.0f seems to boot into some sort of buggy empty workspace.
Click on the workspace button in the top-right to fix.

![Screenshot](/img/buggy-0.1.0f.jpg)
